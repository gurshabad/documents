# Human Rights Review Team

Welcome! This is the documents and organization page for the Human Rights Protocol Considerations Review Team. We'll be doing reviews of Internet-Drafts (RFC 8280, section 6, and 
draft-tenoever-hrpc-guidelines)

- The mailing list: https://www.ietf.org/mailman/listinfo/Hr-rt
- Guidelines for human rights considerations: https://tools.ietf.org/html/draft-irtf-hrpc-guidelines-00
- Last call list: https://datatracker.ietf.org/doc/search/?name=&sort=&rfcs=on&activedrafts=on&by=state&state=15&substate=
- Publication request list: https://datatracker.ietf.org/doc/search/?name=&sort=&rfcs=on&activedrafts=on&by=state&state=16&substate=

## To suggest a new draft for analysis:
- Bring the draft up on the hr-rt mailinglist
- Create a new issue! Use the Suggested-Draft template while creating the issue.

## To add a new analysis:
Simply add the file to the top-level of the project. 

## What format should I use for the analysis?
Generally the questions that are posed in draft-irtf-hrpc-guidelines-00 are answered for the drafts that you are evaluating.

## When you finished your analysis
You can share your analysis with the authors of the draft(s) you analyzed, the relevant Working Group or Research Group for the draft(s), and cc the hr-rt mailinglist.

In terms of uniformity you can consider starting your review of with the following text:

Human Rights reviews are developed by a group of individuals in the IRTF and IETF. They work together to encourage each other to do these detailed reviews as contributions to the IETF open review process. Human Rights reviews are individual contributions. The authors hope that the comments will be taken into consideration by the draft authors, groups and IESG.
